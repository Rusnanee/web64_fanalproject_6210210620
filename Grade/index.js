const mysql = require('mysql')

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'grade',
    password : 'grade',
    database : 'Project_64'
})

connection.connect();

const express = require('express');
const app = express()
const port = 9000

app.post("/add_student", (req, res) => {

    let s_fname = req.query.s_fname
    let s_lname = req.query.s_lname

    let query = ` INSERT INTO student 
                (s_fname, s_laddres)
                VALUES ('${s_fname}',
                        '${s_lname}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into database student"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Adding student succesful"
            })
        }
    });
})

app.post("/add_subject", (req, res) => {

    let sub_subjectId = req.query.sub_subjectId
    let sub_name = req.query.sub_name
    let sub_instructor = req.query.sub_instructor

    let query = ` INSERT INTO subject 
                (sub_subjectId, sub_name, sub_instructor)
                VALUES ('${sub_subjectId}',
                        '${sub_name}'
                        '${sub_instructor}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into database subject"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Adding subject succesful"
            })
        }
    });
})

app.post("/add_grade", (req, res) => {

    let s_ID = req.query.s_ID
    let sub_ID = req.query.sub_ID
    let grade = req.query.grade


    let query = ` INSERT INTO grade 
                (s_ID, sub_ID, grade)
                VALUES ( ${s_ID},
                         ${sub_ID},
                        '${grade}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into database grade"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Adding grade succesful"
            })
        }
    });
})


app.post("/list_student", (req, res) => {
    query = "SELECT * from student";

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error querying from student in database"     
            })
        }
        else{
            res.json(rows)
        }
    });
})

app.post("/list_subject", (req, res) => {
    query = "SELECT * from subject";

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error querying from subject in database"     
            })
        }
        else{
            res.json(rows)
        }
    });
})

app.post("/list_grade", (req, res) => {
    query = "SELECT * from grade";

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error querying from grade in database"     
            })
        }
        else{
            res.json(rows)
        }
    });
})


app.post("/update_student", (req, res) => {

    let s_fname = req.query.s_fname
    let s_lname = req.query.s_lname

    let query = ` UPDATE student SET
                                s_fname='${s_fname}', 
                                s_lname='${s_lname}'`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error updating record"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Updating user succesful"
            })
        }
    });
})

app.post("/update_subject", (req, res) => {

    let sub_subjectId = req.query.sub_subjectId
    let sub_name = req.query.sub_name
    let sub_instructor = req.query.sub_instructor

    let query = ` UPDATE subject SET
                                sub_subjectId='${sub_subjectId}', 
                                sub_name='${sub_name}', 
                                sub_instructor='${sub_instructor}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error updating record"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Updating user succesful"
            })
        }
    });
})

app.post("/update_grade", (req, res) => {

    let s_ID = req.query.s_ID
    let sub_ID = req.query.sub_ID
    let grade = req.query.grade


    let query = ` UPDATE grade SET
                                s_ID=${s_ID}, 
                                sub_ID=${sub_ID}, 
                                grade='${grade}'`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error updating record"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Updating user succesful"
            })
        }
    });
})


app.post("/delete_student", (req, res) => {

    let s_id = req.query.s_id

    let query = ` DELETE FROM student WHERE s_id=${s_id}`
    
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error deleting record"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Deleting event succesful"
            })
        }
    });
})

app.post("/delete_subject", (req, res) => {

    let sub_id = req.query.sub_id

    let query = ` DELETE FROM student WHERE sub_id=${sub_id}`
    
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error deleting record"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Deleting event succesful"
            })
        }
    });
})

app.post("/delete_grade", (req, res) => {

    let g_id = req.query.g_id

    let query = ` DELETE FROM grade WHERE g_id=${g_id}`
    
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error deleting record"     
            })
        }
        else{
            res.json({
                        "status" : "200",
                        "message" : "Deleting event succesful"
            })
        }
    });
})


app.listen(port, () => {
    console.log(` Now starting Reserve BizMall System Backend ${port} `)
})

